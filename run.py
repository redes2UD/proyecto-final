from flask import Flask
from flask import render_template, request
from flask import jsonify
from flask import Flask, session

from huffman.huffman import huffman

app = Flask(__name__, static_folder="static")
app = Flask(__name__, static_url_path="/static")

canAltura = 100
canAnchura = 100

botones = []
simbolos = []


@app.route("/", methods=["GET"])
def catalogo():
    return render_template("catalogo.html")


@app.route("/juego", methods=["GET"])
def juego():
    return render_template("juego.html")


@app.route("/juego/getBotones")
def getBotones():
    btnArriba = "a"
    btnAbajo = "b"
    btnIzquierda = "c"
    btnDerecha = "d"
    btnDiagDerecha = "e"

    texto = btnArriba + btnAbajo + btnIzquierda + btnDerecha
    huff = huffman(texto)

    global botones
    global simbolo

    botones = huff.codigo
    simbolo = huff.simbolo
    return jsonify({"codigo": huff.codigo, "simbolos": huff.simbolo})


@app.route("/juego/getPosicion", methods=["GET"])
def getPosicion(boton=None, xActual=None, yActual=None):
    # velocidad = 256
    global botones
    velocidad = 300
    factorDeAvance = 60 / 1000
    boton = request.args.get("boton")
    xActual = int(request.args.get("xActual"))
    yActual = int(request.args.get("yActual"))
    # canAltura = int(request.args.get("alturaCanvas"))
    # canAnchura = int(request.args.get("anchuraCanvas"))
    # Arriba
    if boton == botones[0]:
        if yActual > 0:
            yActual = yActual - velocidad * factorDeAvance
        else:
            yActual = session["alturaCanvas"] - 32
    # Abajo
    elif boton == botones[1]:
        yActual = (yActual + velocidad * factorDeAvance) % session["alturaCanvas"]

    # IZQUIERDA
    elif boton == botones[2]:
        if xActual > 0:
            xActual = xActual - velocidad * factorDeAvance
        else:
            xActual = session["anchuraCanvas"] - 32
    # Derecha
    elif boton == botones[3]:
        xActual = (xActual + velocidad * factorDeAvance) % session["anchuraCanvas"]
    return jsonify({"x": int(xActual), "y": int(yActual)})


@app.route("/juego/actualizarCanvas", methods=["GET"])
def pruebas():
    session["alturaCanvas"] = int(request.args.get("alturaCanvas"))
    session["anchuraCanvas"] = int(request.args.get("anchuraCanvas"))
    return "True"


@app.route("/huffman", methods=["GET"])
def h():
    texto = request.args.get("texto")
    huff = huffman(texto)
    str1 = ""
    return str(
        "ENCRIPTADO:"
        + huff.encriptado
        + " CODIGO:"
        + str(huff.codigo)
        + " CODIGO:"
        + str(huff.simbolo)
    )


app.secret_key = "1234"
app.run(debug=True)
