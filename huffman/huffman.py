class huffman:
    def __init__(self, mensaje):
        aux = mensaje

        self.simbolo = []
        self.frecuencia = []
        self.padre = []
        self.tipo = []
        self.izq = []
        self.der = []
        self.codigo = []

        letras = ""
        frec = ""
        tamAnt = len(aux)
        while aux != "":
            letras += aux[0]
            aux = aux.replace(aux[0], "",)
            frec += str(tamAnt - len(aux)) + " "
            tamAnt = len(aux)

        a = list(letras)

        frecs = frec.split(" ")

        for i in range(len(a) - 1):
            for j in range(len(a) - i - 1):
                if a[j + 1] < a[j]:
                    aux1 = a[j + 1]
                    aux2 = frecs[j + 1]

                    frecs[j + 1] = frecs[j]
                    a[j + 1] = a[j]

                    a[j] = aux1
                    frecs[j] = aux2

        self.tamanio = len(letras)

        for i in range(self.tamanio):
            self.simbolo.insert(i, a[i])
            self.frecuencia.insert(i, int(frecs[i]))
            self.padre.insert(i, 0)
            self.tipo.insert(i, 0)
            self.der.insert(i, -1)
            self.izq.insert(i, -1)

        for i in range(self.tamanio, 2 * self.tamanio - 1):
            self.simbolo.insert(i, " ")

            # buscando las posiciones de las 2 frecuencias menores
            menor = -1
            for j in range(i):
                if self.padre[j] == 0:
                    menor = j
                    break

            for j in range(menor + 1, i):
                if self.padre[j] == 0 and self.frecuencia[j] < self.frecuencia[menor]:
                    menor = j

            sMenor = self.buscarSegundoMenor(menor, i)

            self.frecuencia.insert(i, self.frecuencia[menor] + self.frecuencia[sMenor])

            self.padre.insert(i, 0)
            self.padre[menor] = i
            self.padre[sMenor] = i

            self.tipo.insert(i, 0)
            self.tipo[menor] = 1
            self.tipo[sMenor] = 2

            self.izq.insert(i, menor)
            self.der.insert(i, sMenor)

        self.generarDic()

        self.encriptado = ""

        for i in range(len(mensaje)):
            for j in range(self.tamanio):
                if mensaje[i] == self.simbolo[j]:
                    self.encriptado += self.codigo[j]

    # método para buscar la posición de la segunda frecuencia menor
    def buscarSegundoMenor(self, pos, tam):
        p = -1

        for j in range(tam):
            if self.padre[j] == 0 and j != pos:
                p = j
                break

        for i in range(p + 1, tam):
            if (
                i != pos
                and self.padre[i] == 0
                and self.frecuencia[i] < self.frecuencia[p]
            ):
                p = i

        return p

    def generarDic(self):
        self.codigo = []

        for i in range(self.tamanio):
            aux = i
            self.codigo.insert(i, "")

            while self.padre[aux] != 0:
                print(self.padre[aux])
                if self.tipo[aux] == 1:
                    self.codigo[i] = "0" + self.codigo[i]
                else:
                    self.codigo[i] = "1" + self.codigo[i]
                aux = self.padre[aux]
